from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client
from . import views

# Create your tests here.
class SampleTest(TestCase):
    
    def test_homepage_story9(self):
        response = Client().get('/story9')
        self.assertEqual(response.status_code,200, "YEY")
        print("story9 : 200")

    def test_story9_using_right_func(self):
        found = resolve('/story9')
        self.assertEqual(found.func, views.story9)
        print("story9 : views func")

    def test_template_used(self):
        response = Client().get('/story9')
        self.assertTemplateUsed(response, 'pages/story9.html')
        print("story9 : test template")

