from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
import requests

# Create your views here.

def story9(request):
    return render(request,'pages/story9.html',{})

def computers_data(request):
    raw = requests.get("https://enterkomputer.com/api/product/notebook.json").json()
    lst_json=[]
    for computer in raw:
        if ("Intel" in computer["details"]) and ("Asus" in computer["name"]):
            lst_json.append(computer)

    lst_real = []
    for i in range(100):
        computer_dict = {"id":lst_json[i]["id"],"name":lst_json[i]["name"],"details":lst_json[i]["details"],"brand":lst_json[i]["brand"], "price":lst_json[i]["price"]}
        lst_real.append(computer_dict)
    return JsonResponse({'data':lst_real})