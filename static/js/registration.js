$(document).ready(function(){
    $('#subs-button').closest('form')
    .submit(function() {
        return checkForm.apply($(this).find(':input')[0]);
    })
    .find(inputSelector).keyup(checkForm).keyup();
  
    $('#subsform').submit(function(e){
        e.preventDefault();
        $.ajax({
            method:'POST',
            url:'/Save_Subscribers/',
            data:$('form').serialize(),
            success:function(status){
                if (status.status == 'True') {
                    $('#buat-alert').html("<div class='alert alert-success' role='alert'>Thank you for subscribing! Have a good day:)</div>");
                }else{
                    $('#buat-alert').html("<div class='alert alert-danger' role='alert'>This email is already been used to subscribe</div>");
                }
            }
    
        });
        return false;
    });

    $('#daftar-subs').click(function(){
        $("#daftar-subs").attr("disabled", true);
        $.ajax({
            url:'/Show_Subscribers/',
            type:'GET',
            dataType:'json',
            success: function(data){
                var row = ('<tr>');
                for(var i = 0;i<data.subsdata.length;i++){
                    row+= '<th scope ="col">' + (i+1) +'</th>';
                    row+= '<td scope ="col">' + data.subsdata[i].name +'</t>';
                    row+= '<td>'+data.subsdata[i].email+'</td>';
                    row+= '<td>' +'<button class="btn btn-default" id="unsubs-button" onClick="deleteSubs('+data.subsdata[i].id+')"type="submit">Unsubscribe</button></td></tr>';
                }
                console.log(row);
                $('#subs-table').append(row);
            }
        });
    })   
});

const inputSelector = ':input[Required]:visible';
function checkForm() {
    var isValidForm = true;
    $(this.form).find(inputSelector).each(function() {
    if (!this.value.trim()) {
        isValidForm = false;
    }
    });
    $(this.form).find('#subs-button').prop('disabled', !isValidForm);
    return isValidForm;
}

function deleteSubs(id){
    console.log(id)
    $.ajax({
        method:'POST',
        url:'/Delete_Subscriber/',
        data:{'id':id},
        success:function(){
            if (confirm("Yakin mau unsubscribe?:(")){
                window.location.reload();
            }
            else
                {return false};
        }
    });
}
