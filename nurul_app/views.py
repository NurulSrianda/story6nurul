
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.db import IntegrityError
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User

from .models import Message
from .models import Subscriber

from .forms import MessageForm
from .forms import SubscriberForms
from datetime import datetime

response={}

# Create your views here.
def home(request):
    data = Message.objects.all().values()

    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            status = Message()
            status.pesan = form.cleaned_data['your_message']
            status.date = datetime.now()
            status.save()
            return HttpResponseRedirect('/')
    else:
        form = MessageForm()

    response = {
        'form' : form,
        'data' : data,
    }
    return render(request, 'pages/index.html', response)

def profile(request):
    return render(request,'pages/profile.html')

def registration(request):
    form = SubscriberForms(request.POST)
    return render(request,'pages/registration.html',{'form':form})

def save_subscribers(request):
    response['form'] = SubscriberForms()
    form = SubscriberForms(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        status = 'True'
        #Store account into subscribers database
        try:
            storedata = Subscriber(
            name=response['name'],
            email=response['email'],
            password=response['password']
            )
            storedata.save()
            return JsonResponse({'status':status})
        except IntegrityError as e:
            status = 'False'
            return JsonResponse({'status':status})        
    status = 'False'
    return JsonResponse({'status':status})

def show_subscribers(request):
    data = list(Subscriber.objects.all().values('id','name','email','password'))
    return JsonResponse({'subsdata':data})

@csrf_exempt
def delete_subscribers(request):
    if(request.method == 'POST'):
        subsdata = request.POST['id']
        Subscriber.objects.filter(id=subsdata).delete()
        return HttpResponse({'status':'Success'})
    else:
        return HttpResponse({'status':'Failed to load page'})

def logout(request):
    request.session.flush()
    auth_logout(request)
    return HttpResponseRedirect('/')
