from django import forms
from .models import Message

class MessageForm(forms.Form):
    your_message = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form',
        'required':'True',
        'placeholder':'Apa kabar?',
    }))

class SubscriberForms(forms.Form):
    error_messages={
        'required':'Required Field'
    }
    name_attrs={
        'type':'text',
        'class':'form-control',
        'placeholder': 'Required'
    }
    email_attrs={
        'type':'email',
        'class': 'form-control',
        'placeholder':'Required'
    }
    password_attrs={
        'type':'password',
        'class':'form-control',
        'placeholder':'Required'
    }

    name = forms.CharField(
        label='Name',
        required = True, 
        widget=forms.TextInput(attrs=name_attrs)
    )

    email = forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs)
    )

    password = forms.CharField(
        label='Password',
        required=True,
        widget=forms.PasswordInput(attrs=password_attrs)
    )