from django.urls import path, include
from django.contrib import admin
from django.contrib.auth import views
from . import views

urlpatterns = [
    path('',views.home,name='home'),
    path('profile/',views.profile,name='profile'),
    path('registration/',views.registration,name='registration'),
    path('Save_Subscribers/',views.save_subscribers, name='Save_Subscribers'),
    path('Show_Subscribers/',views.show_subscribers,name='Show_Subscribers'),
    path('Delete_Subscriber/',views.delete_subscribers,name='Delete_Subscriber'),

    # story 11
    path('logout/', views.logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]