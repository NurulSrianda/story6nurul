from django.test import TestCase
from django.http import HttpRequest
from django.urls import resolve
from django.test import Client
from django.db import IntegrityError

from .models import Message
from .models import Subscriber

from .forms import MessageForm
from .forms import SubscriberForms

from django.utils import timezone

from . import views

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import  unittest

class SampleTest(TestCase):
    # test landing page
    def test_homepage(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200, "YEY")

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.home)

    def test_status_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'pages/index.html')

    # test halaman yang tidak ada
    def test_dummypage(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code,404)

    # test apa kabar
    def test_apakabar(self):
        response = Client().post('/', {})
        self.assertIn("Apa kabar?", response.content.decode())

    def test_create_new_model(self):
        new_message = Message.objects.create(waktu=timezone.now(), pesan ="Hai World")

        counting_all_pesan = Message.objects.all().count()
        self.assertEqual(counting_all_pesan, 1)

    def test_form_validated(self):
        form = MessageForm(data={'pesan':'', 'waktu':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['your_message'],
            ['This field is required.']
        )

    def test_message_completed(self):
        test_str = 'Hai World'
        response_post = Client().post('/', {'pesan':test_str,'waktu':timezone.now})
        self.assertEqual(response_post.status_code,200)
        response = Client().get('/')
        html_response = response.content.decode('UTF-8')

    # test halaman profil
    def test_profile_page(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200, "YEY")

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, views.profile)

    def test_status_template_used_profile(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'pages/profile.html')

    # test ada nama
    def test_nama(self):
        response = Client().post('/profile/', {})
        self.assertIn("Nurul Srianda Putri", response.content.decode())

    # test ada npm
    def test_npm(self):
        response = Client().post('/profile/', {})
        self.assertIn("1806205602", response.content.decode())


class FuncTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        cls.driver = webdriver.Chrome("./chromedriver", chrome_options=chrome_options)
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()
        cls.driver.get("http://127.0.0.1:8000/")

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("test main selesai")

    def test_input_kabar(self):
        kabar = "Tes tes"
        isian = self.driver.find_element_by_name("your_message")
        isian.send_keys(kabar)
        self.driver.find_element_by_id("button").click()
        self.assertIn(kabar, self.driver.page_source)
        print("tes kabar")

#CHALLENGE STORY 7 : 2 test untuk css, 2 test untuk kesesuaian layout

    def test_h3_contain_style(self):
        fontsize = self.driver.find_element_by_tag_name("h3").value_of_css_property("font-size")
        self.assertIn("18.72px", fontsize)
        print("fontsize h3 benar")

    def test_kabar_contain_style(self):
        fontweight = self.driver.find_element_by_name("kabar").value_of_css_property("font-weight")
        fontcolor = self.driver.find_element_by_name("kabar").value_of_css_property("color")
        self.assertIn("700",fontweight)
        self.assertIn("rgba(199, 0, 57, 1)",fontcolor)
        print("fontweight dan fontcolor kabar benar")

    def test_kabar_contain_layout(self):
        kabar_layout = self.driver.find_element_by_name("kabar").text
        self.assertIn("Apa Kabar?", kabar_layout)
        print("kabar ada layout")

    def test_button_contain_layout(self):
        button_layout = self.driver.find_element_by_id("button").text
        self.assertIn("Submit",button_layout)
        print("button ada layout")

    def test_button_profile_to_correct_page(self):
        self.driver.find_element_by_id("profil").click()
        self.assertIn("Nurul Srianda Putri", self.driver.page_source)
        print("benar ke halaman profile")
        self.driver.get("http://127.0.0.1:8000/")

    
#Test halaman profile

    def test_theme_button(self):
        self.driver.find_element_by_id("profil").click()
        button_dark = self.driver.find_element_by_id("Dark").text
        button_light = self.driver.find_element_by_id("Light").text
        self.assertIn("Dark", button_dark)
        self.assertIn("Light", button_light)
        print("theme button ada")
        self.driver.get("http://127.0.0.1:8000/")

    #coba nanti
    #def test_accordion_presents(self):
        #self.driver.find_element_by_id("profil").click()
        #accordion = self.driver.find_element_by_id("choice").text
        #self.assertEquals(accordion, "Aktivitas Saat Ini")
        #print("accordion ada")
        #self.driver.get("http://127.0.0.1:8000/")

#Story 10

class SubscribersTest(TestCase):
    def test_story10_from_validation_for_blank_field(self):
        form=SubscriberForms(data={'name':'', 'email':'', 'password':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['email'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['password'],
            ["This field is required."]
        )
        print("tidak boleh ada yang kosong")

    def test_post_error_and_does_not_exists_in_the_database(self):
        Client().post('/Save_Subscribers/',{'name': ''})
        count_data = Subscriber.objects.all().count()
        self.assertEqual(count_data, 0)
        print("data invalid tidak tersimpan")

    def test_JSON_object_failed_created(self):
        create_model = Subscriber.objects.create(name='saffa',email='laila@saffa.com',password='inipassword')
        self.assertRaises(IntegrityError)

