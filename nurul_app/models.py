from django.db import models
import datetime
# Create your models here.

class Message(models.Model):
    waktu = models.DateField(auto_now_add=True,blank=True)
    pesan = models.TextField(max_length=300)

class Subscriber(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True, db_index=True)
    password = models.CharField(max_length=15)
