from django.contrib import admin

# Register your models here.
from .models import Message
from .models import Subscriber

admin.site.register(Message)
admin.site.register(Subscriber)