$(document).ready(function(){
    let currentbg = $("body").css("background-color");
    let currentText = $("h2").css("color");

    // Accordion
    $("#accordion").accordion();

    $("#Dark").mouseenter(function(){
        $("body").css("background-color","black");
        $("h1").css("color","white");
        $("h2").css("color","white");
        $("h3").css("color","white");
    });
    $("#Dark").mouseleave(function(){
        $("body").css("background-color",currentbg);
        $("h1").css("color",currentText);
        $("h2").css("color",currentText);
        $("h3").css("color",currentText);
    });
    $("#Light").mouseenter( function(){
        $("body").css("background-color","white");
        $("h1").css("color","black");
        $("h2").css("color","black");
        $("h3").css("color","black");
    });
    $("#Light").mouseleave(function(){
        $("body").css("background-color",currentbg);
        $("h1").css("color",currentText);
        $("h2").css("color",currentText);
        $("h3").css("color",currentText);
    });
    $("#Dark").click(function(){
        $("body").css("background-color","black");
        $("body").css("transition","1s");
        $("h1").css("color","white");
        $("h2").css("color","white");
        $("h3").css("color","white");
        currentbg = "black";
        currentText = "white";
    })
    $("#Light").click(function(){
        $("body").css("background-color","white");
        $("h1").css("color","black");
        $("h2").css("color","black");
        $("body").css("transition","1s");
        $("h3").css("color","black");
        currentbg = "white";
        currentText = "black";
    })
})
