$(document).ready(function(){
});

function get_comp(){   
    var csrftoken = $('[name=csrfmiddewaretoken]').val();
    var html;
    $.ajax({
        method: 'GET',
        url: '/render/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        success: function(result){
            let header = "<tr><th>Nomor</th><th>Id</th><th>Name</th><th>Details</th><th>Price</th><th>Mau beli</th></tr><div id='row'></div>";
            $("#tabel").append(header);
            renderlist(result);
            wishlist();
        },
});
};

function renderlist(data){
    let used = data['data'];
    for(i=0;i<used.length;i++){
        var row = "<tr>";
        row += "<td>"+(i+1)+"</td>";
        row += "<td>"+ used[i].id +"</td>";
        row += "<td>"+ used[i].name +"</td>";
        row += "<td>"+ used[i].details +"</td>";
        row += "<td id="+ i +" >"+ used[i].price +"</td>";
        row += "<td>"+"<Button id=" + used[i].id + " class='btn' value="+i+">Add to wishlist</Button>" + "</td>" +"</tr>";
        $("#tabel").append(row);
    };

};

function wishlist(){
    let totalharga = 0;
    $("button").click(function(){
        // Function
        let hargatemp = Number($("#"+$(this).attr("value")).text());
        if($(this).text() === "Add to wishlist"){
            $(this).text("Remove from wishlist");
            totalharga += hargatemp;
            $("#harga").text(totalharga);
            $(this).addClass("red");
 
        }
        else{
            $(this).text("Add to wishlist");
            totalharga -= hargatemp;
            $("#harga").text(totalharga);
            $(this).removeClass("red");
        }
    });
};
